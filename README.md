#### Convolutional Neural Neural Network For Maze Solving

University of Freiburg - WiSe 17/18 - Deep Learning Lab Course - Exercise 03

* Axel Ind (_4563539_)
* Fernando Cristiani (_4527671_)

##### Implementation
* The cnn\_model directory was also commited to the repository in order to store the model after training and allowing to test the neural network withouth training it again.
* `states.csv` and `labels.csv` files were not commited to the repository.
* Branch `master` has the code and training data for the given view size.
* Branch `variable-view-size` has the code and training data for variable view sizes.